# OldBot
### A bot for Discord based on JDA


This is a simple Java based bot made for Discord, a voice and chat software for gamers. This bot uses JDA, a Java wrapper that communicates with Discord API through WebSocket.


The bot is currently purposed to run from an IoT device as a server and it has been tested with Raspberry Pi 3. (over 6 months uptime)


### Chat commands
----
**Commands.java** contains the chat commands:
<br>

- **!help** Show list of commands in chat.


- **!ytube (search parameter)** Searches YouTube and links the first result to the chat.


- **!wiki (search parameter)** Searches Wikipedia English and links the first result to the chat.


- **!wikifi (search parameter)** Searches Wikipedia Finnish and links the first result to the chat.


- **!roll (1-1000 or default)** Rolls a number between a number of your choice. (Default 1-6)


- **!choose (term), (term), ...** Selects randomly from given terms. Split terms with (, ).


- _!json (URL) Gets JSON from the given URL._


- **!ver** Show bot version.



### About
----

**YouTube search** command doesn't use Google API Java imports, but it does a simple and specific self made REST request from YouTube in form of JSON. The bot collects the data and forms it to a link that it posts to the current chat. However, the bot owner still needs a **Google API key** inserted to Ref.java to make the requests.


**Wikipedia search** commands use Wikipedia's already existing "special search" which already functions same way as YouTube search command.


For **choose** command users can insert terms that have multiple words in them. For example Quattro Stagioni and Frutti di mare. Just make sure terms are split with comma and space (, ).


_AudioCommands.java are disabled in this example._

_!json command under construction..._