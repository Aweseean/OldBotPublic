package io.aweseean.personal.discord.bots.oldbot;

import org.json.JSONArray;
import org.json.JSONObject;

public class JsonChecker {
	public void checker(JSONObject jsonObject, JSONArray keys) {
		// key + value t�ytyy selvitt��, nested tai array my�s
		for (int i = 0; i < keys.length(); ++i) {

			String key = keys.getString(i); // Here's your key
			
			String value = jsonObject.optString(key); // Here's your value
			System.out.println(value);
			if (value.startsWith("{")) { // t�ytyy printata nestedin nested key vaan kerran ja lis�t� jokaiseen nest arvoon v�li
				System.out.println(key + ": {"); // Name of the nested object
				String nested = jsonObject.optString(key); // Try creating new JSON object
				JSONObject nestedJson = new JSONObject(nested);
				JSONArray nestedKeys = nestedJson.names();
				JSONObject nestedNames = new JSONObject(); // for adding space in front of nested objects
				for (int j = 0; j < nestedKeys.length(); ++j) { // tarpeellinen??
					String nestedKey = nestedKeys.getString(j);
					StringBuilder sb = new StringBuilder(nestedKey);
					sb.insert(0, " ");
					nestedKey = sb.toString();
					System.out.println(nestedKey);
					nestedNames.put(nestedKey, 0);
					// if last key of nest, print "}" !!!!!
				}
				nestedNames.toJSONArray(nestedKeys);
				checker(nestedJson, nestedKeys);
			} else if (value.startsWith("[")) {
				String nested = jsonObject.optString(key);
				JSONArray nestedArray = new JSONArray(nested);
				System.out.println(nestedArray);
			} else {
				System.out.println(key + ": " + value);
			}
		}
	}

	public void nestedChecker(JSONObject jsonObject, JSONArray keys, String key, String value) {
		int count = 0;
		int maxTries = 3;
		while (true) {
			try {
				value = jsonObject.optString(key); // Here's your value
				System.out.println(key + ": " + value);
				break;
			} catch (Exception e) {
				if (++count == maxTries) throw e;
				System.out.println(key);
				String nested = jsonObject.optString(key); // Try creating new JSON object
				JSONObject nestedJson = new JSONObject(nested);
				JSONArray nestedKeys = nestedJson.names();
				checker(nestedJson, nestedKeys);
			}
			// JSONArray nestedArray = keys.getJSONArray(i); // Try creating new JSON array
			/*
			 * catch (Exception e){ JSONObject nestedJson = keys.getJSONObject(i);
			 * nestedChecker(jsonObject, keys, key, value); }
			 */
			// boolean exists = jsonObject.has(key);
		}
	}
}
