package io.aweseean.personal.discord.bots.oldbot;

import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.exceptions.*;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import javax.security.auth.login.LoginException;

public class OldBot extends ListenerAdapter {

	public static void main(String[] args) throws LoginException, RateLimitedException, InterruptedException {

		JDA jda = new JDABuilder(AccountType.BOT).setToken(Ref.TOKEN).buildBlocking(); // Remember token!
		jda.addEventListener(new Commands());
		// jda.addEventListener(new AudioCommands()); ---- not in use currently
	}
}
