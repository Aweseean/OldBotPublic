package io.aweseean.personal.discord.bots.oldbot;

import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.source.youtube.YoutubeAudioSourceManager;
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException;
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;

import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.VoiceChannel;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.exceptions.PermissionException;

// A lot copied from github Yui, discord music bot

public class AudioCommands extends ListenerAdapter {
	public static final int DEFAULT_VOLUME = 35; // (0 - 150, where 100 is default max volume)

	private final AudioPlayerManager playerManager;
	private final Map<String, ServerAudioManager> musicManagers;

	public AudioCommands() {
		this.playerManager = new DefaultAudioPlayerManager();
		playerManager.registerSourceManager(new YoutubeAudioSourceManager());

		musicManagers = new HashMap<String, ServerAudioManager>();
	}

	@Override
	public void onMessageReceived(MessageReceivedEvent event) {
		String[] command = event.getMessage().getContentDisplay().split(" ");

		Guild guild = event.getGuild();
		ServerAudioManager mng = getMusicManager(guild);
		AudioPlayer player = mng.player;
		TrackScheduler scheduler = mng.scheduler;

		if (!command[0].startsWith(Ref.PREFIX)) {
			return;
		}
		if (command[0].equalsIgnoreCase("!join")) {
			if (command.length < 2) {
				event.getChannel().sendMessage("Error. Enter channel name.").queue();
			} else {
				VoiceChannel chan = guild.getVoiceChannelsByName(command[1], true).stream().findFirst().orElse(null);
				if (chan == null) {
					event.getChannel().sendMessage("Could not find VoiceChannel by name: " + command[1]).queue();
				} else {
					guild.getAudioManager().setSendingHandler(mng.sendHandler);

					try {
						guild.getAudioManager().openAudioConnection(chan);
					} catch (PermissionException e) {
						if (e.getPermission() == Permission.VOICE_CONNECT) {
							event.getChannel()
									.sendMessage("OldBot does not have permission to connect to: " + chan.getName())
									.queue();
						}
					}
				}
			}
		} else if (command[0].equalsIgnoreCase("!leave")) {
			guild.getAudioManager().setSendingHandler(null);
			guild.getAudioManager().closeAudioConnection();
			event.getChannel().sendMessage(":wave:").queue();
		} else if (command[0].equalsIgnoreCase("!play")) {
			if (command.length == 1) // It is only the command to start playback (probably after pause)
			{
				if (player.isPaused()) {
					player.setPaused(false);
					event.getChannel().sendMessage("Playback as been resumed.").queue();
				} else if (player.getPlayingTrack() != null) {
					event.getChannel().sendMessage("Player is already playing!").queue();
				} else if (scheduler.queue.isEmpty()) {
					event.getChannel()
							.sendMessage("The current audio queue is empty! Add something to the queue first!").queue();
				}
			} else { // Commands has 2 parts, .play and SEARCH.
				StringBuilder sb = new StringBuilder(); // Double usage, reset between 1. get number of parameters 2.
				// Get JSON of URL to string
				for (int i = 1; i < command.length; i++) { // Check the amount of parameters and split them with
															// underscore
					if (i > 1) {
						sb.append("_");
					}
					String param = command[i];
					sb.append(param);
				}
				String inputLine = "";
				try {
					URL url = new URL(
							"https://www.googleapis.com/youtube/v3/search?part=id&maxResults=1&order=relevance&q="
									+ sb.toString() + "&type=video&key=" + Ref.GOOGLE_APIKEY);
					InputStream is = url.openStream();
					BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
					sb.setLength(0); //
					while ((inputLine = in.readLine()) != null)
						sb.append(inputLine);
					is.close();
					in.close();
				} catch (MalformedURLException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					JSONObject json = new JSONObject(sb.toString());
					JSONArray arr = json.getJSONArray("items");
					sb.setLength(0);
					for (int i = 0; i < arr.length(); i++) {
						sb.append(arr.getJSONObject(i));
					}
					JSONObject json_id = new JSONObject(sb.toString());
					String videoId = json_id.getJSONObject("id").getString("videoId");
					String youtubeUrl = "https://www.youtube.com/watch?v=" + videoId;
					loadAndPlay(mng, event.getChannel(), youtubeUrl, false);
				}
			}
		} else if (command[0].equalsIgnoreCase("!pause")) {
			if (player.getPlayingTrack() == null) {
				event.getChannel().sendMessage("Cannot pause or resume player because no track is loaded for playing.")
						.queue();
				return;
			}

			player.setPaused(!player.isPaused());
			if (player.isPaused())
				event.getChannel().sendMessage("The player has been paused.").queue();
			else
				event.getChannel().sendMessage("The player has resumed playing.").queue();
		} else if (command[0].equalsIgnoreCase("!stop")) {
			scheduler.queue.clear();
			player.stopTrack();
			player.setPaused(false);
			event.getChannel().sendMessage("Playback has been completely stopped and the queue has been cleared.")
					.queue();
		} else if (command[0].equalsIgnoreCase("!skip")) {
			scheduler.nextTrack();
			event.getChannel().sendMessage("The current track was skipped.").queue();
		} else if (command[0].equalsIgnoreCase("!np")) {
			AudioTrack currentTrack = player.getPlayingTrack();
			if (currentTrack != null) {
				String title = currentTrack.getInfo().title;
				String position = getTimestamp(currentTrack.getPosition());
				String duration = getTimestamp(currentTrack.getDuration());

				String nowplaying = String.format("**Playing:** %s\n**Time:** [%s / %s]", title, position, duration);

				event.getChannel().sendMessage(nowplaying).queue();
			} else
				event.getChannel().sendMessage("The player is not currently playing anything!").queue();
		} else if (command[0].equalsIgnoreCase("!volume")) {
			if (command.length == 1) {
				event.getChannel().sendMessage("Current player volume: **" + player.getVolume() + "**").queue();
			} else {
				try {
					int newVolume = Math.max(10, Math.min(100, Integer.parseInt(command[1])));
					int oldVolume = player.getVolume();
					player.setVolume(newVolume);
					event.getChannel()
							.sendMessage("Player volume changed from `" + oldVolume + "` to `" + newVolume + "`")
							.queue();
				} catch (NumberFormatException e) {
					event.getChannel().sendMessage("`" + command[1] + "` is not a valid integer. (10 - 100)").queue();
				}
			}
		}
	}

	private void loadAndPlay(ServerAudioManager mng, final MessageChannel channel, final String trackUrl,
			final boolean addPlaylist) {
		playerManager.loadItemOrdered(mng, trackUrl, new AudioLoadResultHandler() {
			@Override
			public void trackLoaded(AudioTrack track) {
				String msg = "Adding to queue: " + track.getInfo().title;
				if (mng.player.getPlayingTrack() == null)
					msg += "\nand the Player has started playing;";

				mng.scheduler.queue(track);
				channel.sendMessage(msg).queue();
			}

			@Override
			public void playlistLoaded(AudioPlaylist playlist) {
				AudioTrack firstTrack = playlist.getSelectedTrack();
				List<AudioTrack> tracks = playlist.getTracks();

				if (firstTrack == null) {
					firstTrack = playlist.getTracks().get(0);
				}

				if (addPlaylist) {
					channel.sendMessage("Adding **" + playlist.getTracks().size() + "** tracks to queue from playlist: "
							+ playlist.getName()).queue();
					tracks.forEach(mng.scheduler::queue);
				} else {
					channel.sendMessage("Adding to queue " + firstTrack.getInfo().title + " (first track of playlist "
							+ playlist.getName() + ")").queue();
					mng.scheduler.queue(firstTrack);
				}
			}

			@Override
			public void noMatches() {
				channel.sendMessage("Nothing found by " + trackUrl).queue();
			}

			@Override
			public void loadFailed(FriendlyException exception) {
				channel.sendMessage("Could not play: " + exception.getMessage()).queue();
			}
		});
	}

	private ServerAudioManager getMusicManager(Guild guild) {
		String guildId = guild.getId();
		ServerAudioManager mng = musicManagers.get(guildId);
		if (mng == null) {
			synchronized (musicManagers) {
				mng = musicManagers.get(guildId);
				if (mng == null) {
					mng = new ServerAudioManager(playerManager);
					mng.player.setVolume(DEFAULT_VOLUME);
					musicManagers.put(guildId, mng);
				}
			}
		}
		return mng;
	}

	private static String getTimestamp(long milliseconds) {
		int seconds = (int) (milliseconds / 1000) % 60;
		int minutes = (int) ((milliseconds / (1000 * 60)) % 60);
		int hours = (int) ((milliseconds / (1000 * 60 * 60)) % 24);

		if (hours > 0)
			return String.format("%02d:%02d:%02d", hours, minutes, seconds);
		else
			return String.format("%02d:%02d", minutes, seconds);
	}
}
