package io.aweseean.personal.discord.bots.oldbot;

import java.io.InputStreamReader;
import java.awt.Color;
import java.io.BufferedReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.dv8tion.jda.core.hooks.ListenerAdapter;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.ChannelType;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

import org.json.JSONArray;
import org.json.JSONObject;

public class Commands extends ListenerAdapter {

	@Override
	public void onMessageReceived(MessageReceivedEvent event) {
		if (event.isFromType(ChannelType.PRIVATE)) {
			System.out.printf("[PM] %s: %s\n", event.getAuthor().getName(), event.getMessage().getContentDisplay());
		} else {
			System.out.printf("[%s][%s] %s: %s\n", event.getGuild().getName(), event.getTextChannel().getName(),
					event.getMember().getEffectiveName(), event.getMessage().getContentDisplay());
		}

		String[] command = event.getMessage().getContentDisplay().split(" ");
		String msg = "";

		if (!command[0].startsWith(Ref.PREFIX)) {
			return;
		}

		if (command[0].equalsIgnoreCase("!help")) {

			EmbedBuilder eb = new EmbedBuilder();
			eb.setColor(Color.red);
			eb.setTitle("OldBot Help", null);
			eb.setDescription("----------------");
			eb.addField("Commands:", "----------------", false);
			eb.addField("!ytube (search)", "Searches YouTube, links the first result.", false);
			eb.addField("!wiki (search)", "Searches Wikipedia, links the first result.", false);
			eb.addField("!wikifi (search)", "Wikipedia Suomi -haku.", false);
			eb.addField("!roll (number)", "Rolls a number of your choice from 1-1000. (Default 1-6)", false);
			eb.addField("!choose (term), (term), ...", "Chooses between given terms. Split with (, ).", false);
			eb.addField("!ver", "Show version.", false);
			// Music commands are at JiggaBot
			event.getChannel().sendMessage(eb.build()).queue();
		}
		if (command[0].equalsIgnoreCase("!ver")) {
			msg = "This is Oldbot version " + Ref.VERSION + "!";
			event.getChannel().sendMessage(msg).queue();
		}
		if (command[0].equalsIgnoreCase("!roll")) {
			Random rand = new Random();
			if (command.length < 2) {
				int rollDefault = rand.nextInt(6) + 1; // Results in 1 - 6 (instead of 0 - 5)
				event.getChannel()
						.sendMessage(event.getMember().getEffectiveName() + " rolled: " + rollDefault + " (1-6)")
						.queue();
			} else if (command[1].equalsIgnoreCase("0")) {
				event.getChannel()
						.sendMessage(event.getMember().getEffectiveName() + " rolled: 0. Nice roll man/woman.").queue();
			} else if (!command[1].matches("[0-9]+")) {
				msg = ":upside_down:";
				event.getChannel().sendMessage(msg).queue();
				event.getChannel().sendMessage(event.getMember().getEffectiveName() + " did a barrel roll!").queue();
			} else {
				int number = Integer.parseInt(command[1]);
				if (number <= 1000) {
					int roll = rand.nextInt(number) + 1; // Results in 1 - 6 (instead of 0 - 5)
					event.getChannel()
							.sendMessage(
									event.getMember().getEffectiveName() + " rolled: " + roll + " (1-" + number + ")")
							.queue();
				} else {
					msg = "Error. You can only roll between 1-1000. (Default is 1-6)";
					event.getChannel().sendMessage(msg).queue();
				}

			}
		}
		if (command[0].equalsIgnoreCase("!choose")) {
			Random r = new Random();
			if (command.length < 2) {
				msg = "Choose your destiny He-Man!";
				event.getChannel().sendMessage(msg).queue();
			} else {
				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < command.length; i++) {
					String terms = command[i];
					sb.append(terms);
				}
				String termsAll = sb.toString(); // Using stringbuilder to check the whole message if contains comma
				if (!termsAll.contains(",")) {
					msg = "Error. Enter at least 2 terms! Split with commas (, ).";
					event.getChannel().sendMessage(msg).queue();
				} else {
					String[] term = event.getMessage().getContentDisplay().split(", ");
					term[0] = term[0].replace("!choose ", ""); // Deletes "!choose" from the first parameter
					int select = r.nextInt(term.length);
					msg = "I CHOOSE: " + term[select] + "!";
					event.getChannel().sendMessage(msg).queue();
				}
			}
		}
		if (command[0].equalsIgnoreCase("!wiki")) {
			if (command.length < 2) {
				msg = "Please enter a search parameter...";
				event.getChannel().sendMessage(msg).queue();
			} else {
				StringBuilder sb = new StringBuilder();
				for (int i = 1; i < command.length; i++) { // Check the amount of parameters and split them with
															// underscore
					if (i > 1) {
						sb.append("_");
					}
					String param = command[i];
					sb.append(param);
				}
				msg = "https://en.wikipedia.org/w/index.php?search=" + sb.toString() + "&title=Special%3ASearch";
				event.getChannel().sendMessage(msg).queue();
			}
		}
		if (command[0].equalsIgnoreCase("!wikifi")) {
			if (command.length < 2) {
				msg = "Sy�t� hakusana...";
				event.getChannel().sendMessage(msg).queue();
			} else {
				StringBuilder sb = new StringBuilder();
				for (int i = 1; i < command.length; i++) { // Check the amount of parameters and split them with
															// underscore
					if (i > 1) {
						sb.append("_");
					}
					String param = command[i];
					sb.append(param);
				}
				msg = "https://fi.wikipedia.org/w/index.php?search=" + sb.toString() + "&title=Special%3ASearch";
				event.getChannel().sendMessage(msg).queue();
			}
		}
		if (command[0].equalsIgnoreCase("!ytube")) { // Youtube search command! Links the first video of a search to the
														// chat.
			if (command.length < 2) {
				msg = "Please enter a search parameter...";
				event.getChannel().sendMessage(msg).queue();
			} else {
				StringBuilder sb = new StringBuilder(); // Double usage, reset between 1. get number of parameters 2.
														// Get JSON of URL to string
				for (int i = 1; i < command.length; i++) { // Check the amount of parameters and split them with
															// underscore
					if (i > 1) {
						sb.append("_");
					}
					String param = command[i];
					sb.append(param);
				}
				String inputLine = "";
				try {
					String ue = URLEncoder.encode(sb.toString(), "UTF-8");
					URL url = new URL(
							"https://www.googleapis.com/youtube/v3/search?part=id&maxResults=1&regionCode=FI&order=relevance&q="
									+ ue + "&type=video&key=" + Ref.GOOGLE_APIKEY);
					BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
					sb.setLength(0); //
					while ((inputLine = in.readLine()) != null)
						sb.append(inputLine);
					in.close();
				} catch (MalformedURLException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					JSONObject json = new JSONObject(sb.toString());
					JSONArray arr = json.getJSONArray("items");
					sb.setLength(0);
					for (int i = 0; i < arr.length(); i++) {
						sb.append(arr.getJSONObject(i));
					}
					JSONObject json_id = new JSONObject(sb.toString());
					String videoId = json_id.getJSONObject("id").getString("videoId");
					msg = "https://www.youtube.com/watch?v=" + videoId;
					event.getChannel().sendMessage(msg).queue();
				}
			}
		}
		if (command[0].equalsIgnoreCase("!json")) { // json reader

			if (command.length < 2) {
				msg = "Please enter a JSON link...";
				event.getChannel().sendMessage(msg).queue();
			} else if (command.length > 2) {
				msg = "Please enter only one JSON link parameter...";
				event.getChannel().sendMessage(msg).queue();
			} else {
				final String URL_REGEX = "^((https?)://|(www)\\.)?[a-z0-9-]+(\\.[a-z0-9-]+)+([/?].*)?$";

				Pattern p = Pattern.compile(URL_REGEX);
				Matcher m = p.matcher(command[1]); // URL string should be here
				if (!m.find()) {
					msg = "Please enter an proper URL...";
					event.getChannel().sendMessage(msg).queue();
				} else {

					StringBuilder sb = new StringBuilder();
					String inputLine = "";
					try {
						URL url = new URL(command[1]);
						BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
						sb.setLength(0); //
						while ((inputLine = in.readLine()) != null)
							sb.append(inputLine);
						in.close();
					} catch (MalformedURLException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					} finally {
						JSONObject json = new JSONObject(sb.toString());

						JSONArray keys = json.names();

						JsonChecker jc = new JsonChecker();
						jc.checker(json, keys);
					}
				}
			}
		}
	}
}
