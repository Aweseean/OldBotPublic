package io.aweseean.personal.discord.bots.oldbot;

class Ref {

	static final String TOKEN = ""; // Enter Discord bot token here
	static final String CLIENT_ID = ""; // Enter Discord bot ID here
	static final String GOOGLE_APIKEY = ""; // Enter Google API key here
	static final String PREFIX = "!";
	static final String VERSION = "0.1.3";
}
